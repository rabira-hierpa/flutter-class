import 'package:first_app/products.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import './product_controller.dart';

class ProductManager extends StatefulWidget {
  final Map<String, String> firstProduct;
  ProductManager({this.firstProduct}) {
    print('[ProductManager Widget] Constructor');
  }
  @override
  State<StatefulWidget> createState() {
    print('[ProductManager Widget] createState()');
    return _ProductMangerState();
  }
}

class _ProductMangerState extends State<ProductManager> {
  List<Map<String, String>> _products = [];

  /**
   * This method is similar to the way we pass
   * data from a stateful widget to a stateless widget
   *      _ProductMangerState(firstProduct) {
   *        _products.add(firstProduct);
   *      }
   */

  /**
   * The second method is to use the
   *      initState()
   * whenever we want to access
   * the properties of the parent class.
   * By using this approach you can use
   * widget class that enables you to
   * access the properties of the parent class.
   */

  @override
  void initState() {
    super.initState();
    if (widget.firstProduct != null) {
      _products.add(widget.firstProduct);
    }
    print('[ProductManager Widget] initState()');
  }

  @override
  void didUpdateWidget(ProductManager oldWidget) {
    print('[ProductManager Widget] didUpdateWidget()');
    super.didUpdateWidget(oldWidget);
  }

  // Define the function here and pass it as a reference to the statelesswidget
  void _addProduct(Map<String, String> product) {
    setState(() {
      _products.add(product);
    });
  }

  void _deleteProduct(int index) {
    setState(() {
      _products.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    print('[ProductManager Widget] build()');
    return Column(children: [
      Container(
          margin: EdgeInsets.all(10.0), child: ProductControl(_addProduct)),

      /// Expanded - A widget that expands a child of a Row, Column, or Flex
      /// so that the child fills the available space
      Expanded(child: Products(_products, deleteProduct: _deleteProduct))
      // Another way of displaying the list with limited heigt
      // Container(height:500.0,child:Products(_products))
    ]);
  }
}
