import 'package:first_app/pages/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Products extends StatelessWidget {
  final List<Map<String, String>> products;
  final Function deleteProduct;
  Products(this.products, {this.deleteProduct}) {
    print('[Products Widget] Constructor');
  }

  Widget _buildProductItem(BuildContext context, int index) {
    return Card(
      margin: EdgeInsets.fromLTRB(15.0, 0, 15.0, 20),
      child: Column(
        children: <Widget>[
          Image.asset(products[index]['image']),
          Padding(
            padding: EdgeInsets.all(1.0),
            child: Text(
              products[index]['title'],
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.deepPurple),
            ),
          ),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: Text('Details'),
                color: Theme.of(context).accentColor,
                onPressed: () => Navigator.push<bool>(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => ProductPage(
                            products[index]['title'],
                            products[index]['image']))).then((bool value) => {
                      if (value) {deleteProduct(index)}
                    }),
              )
            ],
          )
        ],
      ),
      elevation: 5.0,
    );
  }

  Widget _buildProductList(BuildContext context) {
    Widget productCard;
    if (products.length > 0) {
      productCard = ListView.builder(
        /// Passing a refernece to the buildPrductItem method
        /// note that it is not executing the method as _buildProductItem()
        itemBuilder: _buildProductItem,
        itemCount: products.length,
        // The total number of itesm in the list
      );
    } else {
      // productCard = Center(
      //     child: Text(
      //   'No items in the list! Click on add product to get started',
      //   textAlign: TextAlign.center,
      //   softWrap: true,
      //   style: TextStyle(fontFamily: 'Ubuntu', color: Colors.green),
      // ));
      productCard = Container();
    }
    return productCard;
  }

  @override
  Widget build(BuildContext context) {
    print('[Products Widget] build()');
    return _buildProductList(context);
  }
}
