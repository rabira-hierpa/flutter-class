import 'package:first_app/pages/products.dart';
import 'package:first_app/product_manager.dart';
import 'package:flutter/material.dart';

class ManageProducts extends StatelessWidget {
  const ManageProducts({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
            child: Column(
          children: <Widget>[
            AppBar(
              title: Text('Pages'),
              automaticallyImplyLeading: false,
            ),
            ListTile(
              leading: Icon(
                Icons.list,
                size: 35,
              ),
              title: Text('All Products'),
              subtitle: Text('Goto page all the products'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => ProudctsPage()));
              },
            )
          ],
        )),
        appBar: AppBar(
          title: Text('Manage Products'),
        ),
        body: Container(
          child: Center(
            child: Text("Manage Products page"),
          ),
        ));
  }
}
