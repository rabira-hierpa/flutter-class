import 'package:first_app/pages/manage-products.dart';
import 'package:first_app/product_manager.dart';
import 'package:flutter/material.dart';

class ProudctsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            AppBar(
              title: Text('Choose'),
              automaticallyImplyLeading: false,
            ),
            ListTile(
              leading: Icon(Icons.archive,
                  size: 35, color: Theme.of(context).primaryColor),
              title: Text('Manage Products'),
              subtitle: Text(
                'Lorem ipsum tex is too long to read',
                style: TextStyle(fontSize: 10),
              ),
              trailing: Icon(
                Icons.info,
                size: 20,
                color: Colors.blue,
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => ManageProducts()));
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Easy List'),
      ),
      body: ProductManager(),
    );
  }
}
