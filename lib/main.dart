import 'package:first_app/pages/auth.dart';
import 'package:first_app/product_manager.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';

// Traditional function method
// void main() {
//   runApp(MyApp());
// }

// Modern arrow function
void main() {
  // debugPaintSizeEnabled = true;
  // debugPaintBaselinesEnabled = true;
  // debugPaintPointersEnabled = true;
  runApp(MyApp());
}

/**
 * Steps to create a StatefulWidget
 * 1. Create a class that extends StatefulWidget
 * 2. In the newly created class define(override) the createState method
 *     class MyApp extends StatefulWidget{
 *      @override
 *          State<StatefulWidget> createState(){
 *            return _MyAppState(); // A new class that will extend the class MyApp
 *          }
 *     }
 * 3. Create a new class that extends the previously created StatefulWidget
 *      class _MyAppState extends State<MyApp>{
 *        //your proprties
 *        @override
 *        Widget build(Buildcontext context){
 *          return MaterialApp(... you content)
 *        }
 *      }
 */
class MyApp extends StatelessWidget {
  final String _starter = 'Another Tester';
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        // debugShowMaterialGrid: true,
        theme: ThemeData(
            brightness: Brightness.light,
            primarySwatch: Colors.deepOrange,
            accentColor: Colors.deepOrangeAccent),
        home: AuthPage());
  }
}
